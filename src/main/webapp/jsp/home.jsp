<%@ page import="rentalsystemcitytransport.model.dto.UserDto" %><%--
  Created by IntelliJ IDEA.
  User: YMS21
  Date: 28.11.2021
  Time: 23:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
</head>
<body>
<% UserDto userDto = (UserDto) request.getSession().getAttribute("user"); %>
<p><%= "Привет, " + userDto.getEmail() + "!" %></p>
<a href="/logout">Выйти</a>
</body>
</html>
