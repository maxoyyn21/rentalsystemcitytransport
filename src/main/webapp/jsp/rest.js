var jwtToken = null;
var irentServiceUrl = 'http://localhost:8080';

class UserService {
    user;

    constructor() {
    }

    loadCurrentUser(successMethod) {
        if(jwtToken) {
            callRestService('/users/current', currentUser => {
                this.user = currentUser;
                successMethod.call();
            });
        } else {
            this.getJwt(jwt => {
                jwtToken = jwt;
                callRestService('/users/current', currentUser => {
                    this.user = currentUser;
                    successMethod.call();
                });
            })
        };
    }

    getJwt(successMethod) {
        $.ajax({
            url: `/jwt`, // вызов сервлета
            success: successMethod,
            error: showErrorToast
        });
    }
}

class ParkingsService {
    constructor() {
    }

    getParkings(successMethod) {
        callRestService('/parkings', successMethod);
    }

    createParking(parking, successMethod) {
        callRestService('/parkings', successMethod, 'POST', parking);
    }

    deleteParking(id, successMethod) {
        callRestService(`/parkings/${id}`, successMethod, 'DELETE');
    }
}

class VehiclesService {
    constructor() {
    }

    getVehicles(successMethod) {
        callRestService('/transports', successMethod);
    }

    createVehicle(vehicle, successMethod) {
        callRestService('/transports', successMethod, 'POST', vehicle);
    }
}

class TripsService {
    constructor() {
    }

    getTrips(successMethod) {
        callRestService('/rents?page=0&size=20', successMethod, 'GET');
    }
}

function callRestService(restUrl, successMethod, method = 'GET', data = null) {
    console.log(`${irentServiceUrl}${restUrl}`);
    $.ajax({
        url: `${irentServiceUrl}${restUrl}`,
        method: method,
        data: JSON.stringify(data),
        headers: {
            "Authorization": `Bearer ${jwtToken}`,
            "Content-Type": 'application/json'
        },
        success: successMethod,
        error: showErrorToast
    });
}

function showErrorToast(error) {
    var errorBody = error.responseJSON;
    var errorMessage = errorBody.message ? errorBody.message : 'Внутренняя ошибка сервиса';

    $('#toast .toast-body').html(errorMessage);
    $('#toast').toast('show');
}