<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Вход</title>
</head>
<body>
  <form action="/login" method="post">
    <h4>Вход</h4>
    <p>
      <%= request.getAttribute("credError") != null ? request.getAttribute("credError") : "" %>
    </p>
    <label>Email</label><br>
    <input name="email" type="email"/><br><br>
    <label>Пароль</label><br>
    <input name="password" type="password"/><br><br>
    <input type="submit" value="Войти">
  </form>
</body>
</html>
