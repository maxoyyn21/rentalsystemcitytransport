package rentalsystemcitytransport.servlet;

import org.springframework.beans.factory.annotation.Autowired;
import rentalsystemcitytransport.model.dto.UserDto;
import rentalsystemcitytransport.service.security.SecurityService;
import rentalsystemcitytransport.service.user.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "JwtServlet", value = "/jwt")
public class JwtServlet extends AbstractServlet{
    @Autowired
    private SecurityService securityService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        UserDto userDto = (UserDto) session.getAttribute("user");

        String jwt = securityService.createJwt(userDto.getId(), userDto.getEmail(), userDto.getRole());

        resp.setStatus(HttpServletResponse.SC_OK);
        resp.getWriter().write(jwt);
        resp.getWriter().flush();
    }
}
