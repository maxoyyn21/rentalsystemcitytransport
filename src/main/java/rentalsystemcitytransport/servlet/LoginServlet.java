package rentalsystemcitytransport.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import rentalsystemcitytransport.model.dto.UserDto;
import rentalsystemcitytransport.model.entity.User;
import rentalsystemcitytransport.model.mapping.UserMapper;
import rentalsystemcitytransport.service.security.SecurityService;
import rentalsystemcitytransport.service.user.UserService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "LoginServlet", value = "/login")
public class LoginServlet extends AbstractServlet {
    @Autowired
    private UserService userService;
    @Autowired
    private SecurityService securityService;

    private final Logger logger = LoggerFactory.getLogger(LoginServlet.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email").trim();
        String password = request.getParameter("password").trim();

        if(email.isEmpty() || password.isEmpty()) {
            logger.info("Пользователь не прошел авторизацию.");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/login.jsp");
            request.setAttribute("credError", "Email и пароль не должны быть пустыми.");
            requestDispatcher.forward(request, response);
        }

        User user = userService.getUserByEmailAndPassword(email, password);

        if(user == null){
            logger.info("Пользователь не прошел авторизацию.");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/login.jsp");
            request.setAttribute("credError", "Неверный email и/или пароль.");
            requestDispatcher.forward(request, response);
        }

        HttpSession session = request.getSession();
        UserDto userDto = UserMapper.MapUserToUserDto(user);
        session.setAttribute("user", userDto);

        logger.debug("Пользователь авторизован.");

        response.sendRedirect("/jsp/main.jsp");
    }
}
