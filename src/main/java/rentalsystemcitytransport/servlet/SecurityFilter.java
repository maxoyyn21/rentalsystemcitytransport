package rentalsystemcitytransport.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rentalsystemcitytransport.model.dto.UserDto;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@WebFilter(filterName = "SecurityFilter", urlPatterns = {"/*"})
public class SecurityFilter implements Filter {
    private final Logger logger = LoggerFactory.getLogger(SecurityFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession session = httpServletRequest.getSession(false);

        List<String> uriParts = Arrays.asList(httpServletRequest.getRequestURI().split("/"));
        uriParts.stream().forEach(x -> x.toLowerCase(Locale.ROOT));

        if(session != null){
            UserDto userDto = (UserDto) session.getAttribute("user");

            if(userDto != null
            || (userDto == null && (uriParts.contains("login.jsp") || uriParts.contains("login")))){
                chain.doFilter(request, response);
            }
            else if(userDto == null){
                logger.warn("Попытка несанкционированного доступа.");
                RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("/jsp/login.jsp");
                requestDispatcher.forward(request, response);
            }
        }
        else{
            logger.warn("Попытка несанкционированного доступа.");
            RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("/jsp/login.jsp");
            requestDispatcher.forward(request, response);
        }
    }
}
