package rentalsystemcitytransport.servlet;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import rentalsystemcitytransport.config.SpringConfig;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "AbstractServlet")
public abstract class AbstractServlet extends HttpServlet {
    protected AutowireCapableBeanFactory ctx;

    @Override
    public void init() throws ServletException {
        super.init();
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        ctx = context.getAutowireCapableBeanFactory();
        ctx.autowireBean(this);
    }
}
