package rentalsystemcitytransport.service.security;

import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Jwts;
import rentalsystemcitytransport.model.enums.UserRoleEnum;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class SecurityServiceImpl implements SecurityService{
    @Value("${jwt.secret}")
    private String secret;

    @Override
    public String createJwt(Long userId, String email, UserRoleEnum userRole) {
        Map<String, Object> claims = new HashMap<>();

        claims.put("id", userId);
        claims.put("email", email);
        claims.put("role", userRole);

        Date now = new Date();
        Date expirationDate = new Date(now.getTime() + 7 * 24 * 60 * 60 * 1000);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(String.valueOf(userId))
                .setIssuedAt(now)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
}
