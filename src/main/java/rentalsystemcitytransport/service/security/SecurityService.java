package rentalsystemcitytransport.service.security;

import rentalsystemcitytransport.model.enums.UserRoleEnum;

public interface SecurityService {
    String createJwt(Long userId, String email, UserRoleEnum userRole);
}
