package rentalsystemcitytransport.service.user;

import rentalsystemcitytransport.model.entity.User;

import java.util.Optional;

public interface UserService {
    User getUserByEmailAndPassword(String email, String password);
}
