package rentalsystemcitytransport.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rentalsystemcitytransport.access.user.UserDao;
import rentalsystemcitytransport.model.entity.User;

@Service
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    @Autowired
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
        System.out.printf("Init bean %s.\n", UserServiceImpl.class);
    }

    @Override
    public User getUserByEmailAndPassword(String email, String password) {
        return userDao.getUserByEmailAndPassword(email, password);
    }
}
