package rentalsystemcitytransport.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@Configuration
@ComponentScan("rentalsystemcitytransport")
@PropertySource("classpath:application.properties")
public class SpringConfig {
    @EventListener(ContextRefreshedEvent.class)
    public void onInitializedOrRefreshedApp() {
        System.out.println("The application has been initialized or refreshed.");
    }
}
