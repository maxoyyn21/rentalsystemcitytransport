package rentalsystemcitytransport.access.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import rentalsystemcitytransport.db.DBConnectionManager;
import rentalsystemcitytransport.model.entity.User;
import rentalsystemcitytransport.model.enums.UserRoleEnum;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Optional;

@Repository
public class UserDaoImpl implements UserDao{
    private DBConnectionManager dbConnectionManager;

    @Autowired
    public UserDaoImpl(@Qualifier("DBConnectionManagerHikariImpl") DBConnectionManager dbConnectionManager) {
        this.dbConnectionManager = dbConnectionManager;
        System.out.printf("Init bean %s.\n", UserDaoImpl.class);
    }

    @Override
    public User getUserByEmailAndPassword(String email, String password){
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        User user = null;

        try{
            conn = dbConnectionManager.getConnection();

            String sql = """
                        SELECT * 
                        FROM user
                        WHERE email = ? AND password = ?
                        """;

            stmt = conn.prepareStatement(sql);

            stmt.setString(1, email);
            stmt.setString(2, password);

            rs = stmt.executeQuery();

            while(rs.next()){
                Long id = rs.getLong("id");
                String telegramUsername = rs.getString("telegram_username");
                String userEmail = rs.getString("email");
                String userPassword = rs.getString("password");
                BigDecimal amount = (BigDecimal) rs.getObject("amount");
                UserRoleEnum role = UserRoleEnum.valueOf(rs.getString("role"));

                user = new User(id, telegramUsername, userEmail, userPassword, amount, role);

                user.setId(id);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            try{
                if(rs != null) {
                    rs.close();
                }
                if(stmt != null) {
                    stmt.close();
                }
                if(conn != null && !conn.isClosed()) {
                    conn.close();
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        return user;
    }
}
