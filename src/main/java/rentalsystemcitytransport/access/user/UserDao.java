package rentalsystemcitytransport.access.user;

import rentalsystemcitytransport.model.entity.User;

public interface UserDao {
    User getUserByEmailAndPassword(String email, String password);
}
