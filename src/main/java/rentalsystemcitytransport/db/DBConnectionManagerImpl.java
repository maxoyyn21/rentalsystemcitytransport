package rentalsystemcitytransport.db;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class DBConnectionManagerImpl implements DBConnectionManager {
    @Value("${db.url}")
    private String url;
    @Value("${db.user}")
    private String user;
    @Value("${db.pass}")
    private String pass;

    public DBConnectionManagerImpl() throws SQLException {
        DriverManager.registerDriver(new org.h2.Driver());
        System.out.printf("Init bean %s.\n", DBConnectionManagerImpl.class);
    }

    @Override
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, user, pass);
    }
}
