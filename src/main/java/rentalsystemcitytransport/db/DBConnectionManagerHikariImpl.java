package rentalsystemcitytransport.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;

@Component
public class DBConnectionManagerHikariImpl implements DBConnectionManager {
    @Value("${db.url}")
    private String url;
    @Value("${db.user}")
    private String user;
    @Value("${db.pass}")
    private String pass;
    @Value("${hikari.cachePrepStmts}")
    private String cachePrepStmts;
    @Value("${hikari.prepStmtCacheSize}")
    private String prepStmtCacheSize;
    @Value("${hikari.prepStmtCacheSqlLimit}")
    private String prepStmtCacheSqlLimit;

    private HikariConfig config;
    private HikariDataSource dataSource;

    public DBConnectionManagerHikariImpl(){
        System.out.printf("Init bean %s.\n", DBConnectionManagerHikariImpl.class);
    }

    @Override
    public synchronized Connection getConnection() throws SQLException {
        if(dataSource == null){
            config = new HikariConfig();
            config.setJdbcUrl(url);
            config.setUsername(user);
            config.setPassword(pass);
            config.addDataSourceProperty( "cachePrepStmts" , cachePrepStmts);
            config.addDataSourceProperty( "prepStmtCacheSize" , prepStmtCacheSize);
            config.addDataSourceProperty( "prepStmtCacheSqlLimit" , prepStmtCacheSqlLimit);
            dataSource = new HikariDataSource(config);
        }

        return dataSource.getConnection();
    }
}
