package rentalsystemcitytransport.model.enums;

public enum TransportConditionEnum {
    GREAT,
    GOOD,
    SATISFACTORY
}
