package rentalsystemcitytransport.model.enums;

public enum TransportRentStatusEnum {
    FREE,
    BUSY
}
