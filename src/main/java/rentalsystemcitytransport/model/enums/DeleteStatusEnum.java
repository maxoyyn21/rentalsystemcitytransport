package rentalsystemcitytransport.model.enums;

public enum DeleteStatusEnum {
    DELETED,
    NOT_DELETED
}
