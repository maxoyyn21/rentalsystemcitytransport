package rentalsystemcitytransport.model.enums;

public enum ParkingTypeEnum {
    ELECTRIC_SCOOTER,
    BICYCLE,
    ELECTRIC_SCOOTER_AND_BICYCLE
}
