package rentalsystemcitytransport.model.enums;

public enum TransportTypeEnum {
    ELECTRIC_SCOOTER,
    BICYCLE
}
