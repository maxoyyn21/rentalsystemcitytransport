package rentalsystemcitytransport.model.enums;

public enum RentStatusEnum {
    OPEN,
    CLOSE
}
