package rentalsystemcitytransport.model.enums;

public enum UserRoleEnum {
    ADMIN,
    USER
}
