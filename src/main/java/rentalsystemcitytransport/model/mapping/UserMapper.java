package rentalsystemcitytransport.model.mapping;

import rentalsystemcitytransport.model.dto.UserDto;
import rentalsystemcitytransport.model.entity.User;

public class UserMapper {
    public static UserDto MapUserToUserDto(User user){
        return new UserDto(
                user.getId(),
                user.getEmail(),
                user.getRole()
        );
    }
}
