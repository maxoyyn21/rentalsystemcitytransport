package rentalsystemcitytransport.model.dto;

import rentalsystemcitytransport.model.enums.UserRoleEnum;

public class UserDto {
    private Long id;
    private String email;
    private UserRoleEnum role;

    public UserDto(Long id, String email, UserRoleEnum role) {
        this.id = id;
        this.email = email;
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRoleEnum getRole() {
        return role;
    }

    public void setRole(UserRoleEnum role) {
        this.role = role;
    }
}
