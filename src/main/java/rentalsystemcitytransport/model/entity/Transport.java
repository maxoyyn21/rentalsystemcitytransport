package rentalsystemcitytransport.model.entity;

import rentalsystemcitytransport.model.enums.*;

public class Transport {
    private int id;
    private String name;
    private Integer charge;
    private Integer maxSpeed;
    private TransportConditionEnum condition;
    private String latitude;
    private String longitude;
    private TransportTypeEnum type;
    private TransportRentStatusEnum rentStatus;
    private int parkingId;
    private DeleteStatusEnum deleteStatus;

    public Transport(int id,
                     String name,
                     Integer charge,
                     Integer maxSpeed,
                     TransportConditionEnum condition,
                     String latitude,
                     String longitude,
                     TransportTypeEnum type,
                     TransportRentStatusEnum rentStatus,
                     int parkingId,
                     DeleteStatusEnum deleteStatus) {
        this.id = id;
        this.name = name;
        this.charge = charge;
        this.maxSpeed = maxSpeed;
        this.condition = condition;
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
        this.rentStatus = rentStatus;
        this.parkingId = parkingId;
        this.deleteStatus = deleteStatus;
    }

    @Override
    public String toString() {
        return "Transport{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", charge=" + charge +
                ", maxSpeed=" + maxSpeed +
                ", condition=" + condition +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", type=" + type +
                ", rentStatus=" + rentStatus +
                ", parkingId=" + parkingId +
                ", deleteStatus=" + deleteStatus +
                '}';
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getCharge() {
        return charge;
    }

    public void setCharge(Integer charge) {
        this.charge = charge;
    }

    public Integer getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Integer maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public TransportConditionEnum getCondition() {
        return condition;
    }

    public void setCondition(TransportConditionEnum condition) {
        this.condition = condition;
    }

    public TransportTypeEnum getType() {
        return type;
    }

    public void setType(TransportTypeEnum type) {
        this.type = type;
    }

    public TransportRentStatusEnum getRentStatus() {
        return rentStatus;
    }

    public void setRentStatus(TransportRentStatusEnum rentStatus) {
        this.rentStatus = rentStatus;
    }

    public int getParkingId() {
        return parkingId;
    }

    public void setParkingId(int parkingId) {
        this.parkingId = parkingId;
    }

    public DeleteStatusEnum getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(DeleteStatusEnum deleteStatus) {
        this.deleteStatus = deleteStatus;
    }
}
