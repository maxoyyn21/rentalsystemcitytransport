package rentalsystemcitytransport.model.entity;

import rentalsystemcitytransport.model.enums.RentStatusEnum;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Rent {
    private int id;
    private int startParkingId;
    private int endParkingId;
    private LocalDateTime startRentDatetime;
    private LocalDateTime endRentDatetime;
    private BigDecimal amount;
    private RentStatusEnum status;
    private int userId;
    private int transportId;

    public Rent(int id,
                int startParkingId,
                int endParkingId,
                LocalDateTime startRentDatetime,
                LocalDateTime endRentDatetime,
                BigDecimal amount,
                RentStatusEnum status,
                int userId,
                int transportId) {
        this.id = id;
        this.startParkingId = startParkingId;
        this.endParkingId = endParkingId;
        this.startRentDatetime = startRentDatetime;
        this.endRentDatetime = endRentDatetime;
        this.amount = amount;
        this.status = status;
        this.userId = userId;
        this.transportId = transportId;
    }

    @Override
    public String toString() {
        return "Rent{" +
                "id=" + id +
                ", startParkingId=" + startParkingId +
                ", endParkingId=" + endParkingId +
                ", startRentDatetime=" + startRentDatetime +
                ", endRentDatetime=" + endRentDatetime +
                ", amount=" + amount +
                ", status=" + status +
                ", userId=" + userId +
                ", transportId=" + transportId +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStartParkingId() {
        return startParkingId;
    }

    public void setStartParkingId(int startParkingId) {
        this.startParkingId = startParkingId;
    }

    public int getEndParkingId() {
        return endParkingId;
    }

    public void setEndParkingId(int endParkingId) {
        this.endParkingId = endParkingId;
    }

    public LocalDateTime getStartRentDatetime() {
        return startRentDatetime;
    }

    public void setStartRentDatetime(LocalDateTime startRentDatetime) {
        this.startRentDatetime = startRentDatetime;
    }

    public LocalDateTime getEndRentDatetime() {
        return endRentDatetime;
    }

    public void setEndRentDatetime(LocalDateTime endRentDatetime) {
        this.endRentDatetime = endRentDatetime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public RentStatusEnum getStatus() {
        return status;
    }

    public void setStatus(RentStatusEnum status) {
        this.status = status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTransportId() {
        return transportId;
    }

    public void setTransportId(int transportId) {
        this.transportId = transportId;
    }
}
