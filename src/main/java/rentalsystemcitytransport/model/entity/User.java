package rentalsystemcitytransport.model.entity;

import rentalsystemcitytransport.model.enums.UserRoleEnum;

import java.math.BigDecimal;

public class User {
    private Long id;
    private String telegramUsername;
    private String email;
    private String password;
    private BigDecimal amount;
    private UserRoleEnum role;

    public User(Long id,
                String telegramUsername,
                String email,
                String password,
                BigDecimal amount,
                UserRoleEnum role) {
        this.id = id;
        this.telegramUsername = telegramUsername;
        this.email = email;
        this.password = password;
        this.amount = amount;
        this.role = role;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getTelegramUsername() {
        return telegramUsername;
    }

    public void setTelegramUsername(String telegramUsername) {
        this.telegramUsername = telegramUsername;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRoleEnum getRole() {
        return role;
    }

    public void setRole(UserRoleEnum role) {
        this.role = role;
    }
}
