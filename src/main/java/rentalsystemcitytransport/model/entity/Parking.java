package rentalsystemcitytransport.model.entity;

import rentalsystemcitytransport.model.enums.DeleteStatusEnum;
import rentalsystemcitytransport.model.enums.ParkingTypeEnum;

public class Parking {
    private int id;
    private String name;
    private String longitude;
    private String latitude;
    private double radius;
    private ParkingTypeEnum type;
    private DeleteStatusEnum deleteStatus;

    public Parking(int id,
                   String name,
                   String longitude,
                   String latitude,
                   double radius,
                   ParkingTypeEnum type,
                   DeleteStatusEnum deleteStatus) {
        this.id = id;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.radius = radius;
        this.type = type;
        this.deleteStatus = deleteStatus;
    }

    @Override
    public String toString() {
        return "Parking{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", radius=" + radius +
                ", type=" + type +
                ", deleteStatus=" + deleteStatus +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public ParkingTypeEnum getType() {
        return type;
    }

    public void setType(ParkingTypeEnum type) {
        this.type = type;
    }

    public DeleteStatusEnum getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(DeleteStatusEnum deleteStatus) {
        this.deleteStatus = deleteStatus;
    }
}
